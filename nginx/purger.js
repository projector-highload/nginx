var fs = require('fs');
var cr = require('crypto');

function test(r) {
    if (r.method !== 'PURGE') {
        r.internalRedirect('@proxy');
        return;
    }

    if (r.uri === '/') {
        try {
            removeEntireCache();
            r.return(200, 'ok');
        } catch (e) {
            r.return(500, e.message);
        }

        return;
    }

    var md5 = cr.createHash('md5');
    md5.update(r.uri);

    var hash = cr.createHash('md5').update(r.uri).digest('hex');
    var bucket = hash[hash.length-1];
    var fileName = '/data/nginx/cache/' + bucket + '/' + hash.toString();

    try {
        fs.accessSync(fileName, fs.constants.R_OK | fs.constants.W_OK);
        fs.unlinkSync(fileName);
        r.return(200, 'ok');
    } catch (e) {
        r.return(404, 'Not found');
    }
}

function removeEntireCache() {
    fs.readdirSync('/data/nginx/cache', {withFileTypes: true}).forEach(function (dirent) {
        var name = '/data/nginx/cache/' + dirent.name;

        if (dirent.isFile()) {
            fs.unlinkSync(name);
            return;
        }

        removeDir(name);
    });
}

function removeDir(dir) {
    fs.readdirSync(dir, {withFileTypes: true}).forEach(function (dirent) {
        var name = dir + '/' + dirent.name;

        if (dirent.isFile()) {
            fs.unlinkSync(name);
            return;
        }

        removeDir(name);
    });

    fs.rmdirSync(dir);
}

export default {test};
