user  nginx;
worker_processes  auto;

error_log  /var/log/nginx/error.log notice;
pid        /var/run/nginx.pid;

load_module modules/ngx_http_js_module.so;

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;
    error_log   /var/log/nginx/error.log warn;

    sendfile        on;

    keepalive_timeout  65;

    proxy_cache_path /data/nginx/cache levels=1 keys_zone=static_cache:10m;

    js_import purger.js;

    server {
        location / {
            js_content purger.test;
        }

        location @proxy {
            proxy_cache static_cache;
            proxy_cache_min_uses 2;
            proxy_cache_valid 200 60m;
            proxy_cache_key "$request_uri";

            proxy_pass http://static;
        }
    }
}
